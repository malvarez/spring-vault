# Vault

The following is an example on how to use *vault* and *spring-cloud-vault* to access the secrets to connect to a *mysql* database.

## How to run

* Start the vault server and the mysql databases

```text
$ docker-compose up
```

* Check that connecting to the server throws an error due to not having the connection data

[http://localhost:8080](http://localhost:8080)


* Add the secrets regarding the first *mysql* node to *vault*

```text
$ docker exec -it --user vault {VAULT_CONTAINER_ID} /bin/sh
$ vault write secret/vault-web spring.datasource.url=jdbc:mysql://localhost:3306/vault \
    spring.datasource.username=root \
    spring.datasource.password=db1
```    

* Do a post to refresh the data source  

[http://localhost:8080/refresh](http://localhost:8080/refresh)

* Update the secrets to connect to the second *mysql*

```text
$ docker exec -it --user vault {VAULT_CONTAINER_ID} /bin/sh
$ vault write secret/vault-web spring.datasource.url=jdbc:mysql://localhost:3307/vault \
    spring.datasource.username=root \
    spring.datasource.password=db2
```

* Do a post to refresh the data source  

[http://localhost:8080/refresh](http://localhost:8080/refresh)


   
   