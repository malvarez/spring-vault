package cern.ais.pm.vault;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VaultWeb {

    public static void main(String[] args) {
        SpringApplication.run(VaultWeb.class, args);
    }
}
