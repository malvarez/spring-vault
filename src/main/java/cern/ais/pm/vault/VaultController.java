package cern.ais.pm.vault;

import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.sql.DataSource;

@Controller
public class VaultController {

    private final DataSource dataSource;

    public VaultController(final DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @GetMapping(value = "/", produces = MediaType.TEXT_PLAIN_VALUE)
    @ResponseBody
    public String getDatabaseVersion() {
        return new JdbcTemplate(dataSource).queryForObject(
                "select version()",
                (rs, rowNum) -> rs.getString(1)
        );
    }
}
